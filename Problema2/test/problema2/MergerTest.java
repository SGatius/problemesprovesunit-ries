/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problema2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author santi
 */
public class MergerTest {
    
    private Merger merger;
    private List<Integer> list1;
    private List<Integer> list2;
    @Before
    public void initMergerTest(){
        merger = new Merger();
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
    }

    @Test
    public void testMergeSortCorrect() {
        list1 = Arrays.asList(1, 2, 3, 7);
        list2 = Arrays.asList(4, 8, 9, 11);
        List<Integer> expected = new ArrayList<>();
        expected = Arrays.asList(1, 2, 3, 4, 7, 8, 9, 11);
        assertEquals(expected, merger.mergeSorted(list1, list2));
    }
    @Test
    public void testMergeSortNotCorrect() {
        list1 = Arrays.asList(1, 2, 7);
        list2 = Arrays.asList(4, 8, 9);
        List<Integer> expected = new ArrayList<>();
        expected = Arrays.asList(1, 2, 3, 4, 5, 6);
        assertNotEquals(expected, merger.mergeSorted(list1, list2));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMergeWithEmptyList(){
        list1 = Collections.emptyList();
        list2 = Arrays.asList(3, 4, 6);
        merger.mergeSorted(list1, list2);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testMergeWithoutSortedList(){
        list1 = Arrays.asList(2, 1, 3);
        list2 = Arrays.asList(3, 4, 6);
        merger.mergeSorted(list1, list2);
    }
}
