/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problema2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author santi
 */
public class Merger {
    public List<Integer> mergeSorted(List<Integer> list1, 
            List<Integer> list2)throws IllegalArgumentException{
        List<Integer> list = new ArrayList<Integer>();
        if(list1.isEmpty() || list2.isEmpty()){
            throw new IllegalArgumentException("Alguna llista buida");
        }
        if(!checkList(list1) || !checkList(list2)){
            throw new IllegalArgumentException("Llista no ordenada");
        }
        else{
            list.addAll(list1);
            list.addAll(list2);
        }
        Collections.sort(list);
        return list;
    }
    public boolean checkList(List<Integer> list){
        Iterator itr = list.iterator();
        Object o;
        while(itr.hasNext()){
            o = itr.next();
            while(itr.hasNext()){
                Object obj = itr.next();
                if(o.equals(obj)){
                    return false;
                }
            }
        }
        return true;
    }
}
